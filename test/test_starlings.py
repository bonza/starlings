import numpy as np
import pytest
from starlings import starlings


ZEROS = np.array([0., 0., 0.])
ONES = np.array([1., 1., 1.])
ONE_DIM = np.array([1., 0., 0.])

def flock_factory(flock_type='flock', dt=1., roost=None, **kwargs):
    """
    """
    # determine inputs provided
    if 'positions' in kwargs:
        positions = kwargs['positions']
        del kwargs['positions']
    else:
        positions = None
    if 'velocities' in kwargs:
        velocities = kwargs['velocities']
        del kwargs['velocities']
    else:
        velocities = None
    # make the flock
    flock = []
    if positions and not velocities:
        for position in positions:
            flock.append(starlings.Starling(position, ZEROS, **kwargs))
    elif not positions and velocities:
        for velocity in velocities:
            flock.append(starlings.Starling(ZEROS, velocity, **kwargs))
    else:
        for position, velocity in zip(positions, velocities):
            flock.append(starlings.Starling(position, velocity, **kwargs))
    if flock_type == 'flock':
        flock = starlings.Flock(flock, dt=dt, roost=roost)
    else: 
        flock = starlings.SubFlock(flock)
    return flock
        
 
@pytest.mark.parametrize('position,velocity,expected', [
    # looking straight at other starling
    (ONE_DIM, ONE_DIM, (1., 0.)),
    # looking at right angles to other starling
    (ONE_DIM, np.array([0., 1., 0.]), (1., np.pi / 2)),
    # looking in opposite direction to other starling
    (ONE_DIM, -1 * ONE_DIM, (1., np.pi)),
    # looking in opposite direction, greater velocity (should be unaffected)
    (ONE_DIM, -2 * ONE_DIM, (1., np.pi)),
    # opposite diection, greater distance
    (2 * ONE_DIM, -1 * ONE_DIM, (2., np.pi)),
    # full-dimensional, looking straight on
    (ONES, ONES, (np.sqrt(3), 0.)),
    # looking in arbitrary direction, full dimensional
    (ONES, np.array([1., 1., 0.]), (np.sqrt(3), np.arctan(1 / np.sqrt(2)))),
    # looking in arbitrary direction, full dimensional, greater velocity
    (ONES, np.array([2., 2., 0.]), (np.sqrt(3), np.arctan(1 / np.sqrt(2)))),
])    
def test_starling_get_direction_to_other(position, velocity, expected):
    """
    """
    # simple subject starling
    starling = starlings.Starling(ZEROS, velocity)
    other = starlings.Starling(position, ZEROS)
    dist, ang = starling.get_direction_to_other(other)
    np.testing.assert_allclose((dist, ang), expected)
    
    
@pytest.mark.parametrize('positions,n,nearest_inds', [
    # five differently positioned starlings
    ([2 * ONE_DIM, ONE_DIM, 3 * ONE_DIM,
      np.array([0., 0.5, 0.]), np.array([0.1, 0.1, 0.1])], 3, [4, 3, 1]
    ),
    # less than `n` starlings available
    ([2 * ONE_DIM, ONE_DIM], 3, [1, 0]),
])    
def test_starling_get_nearest_n(positions, n, nearest_inds):
    """
    """
    # simple subject starling
    starling = starlings.Starling(ZEROS, ONES, n=3)
    flock = flock_factory(flock_type='subflock', positions=positions)
    nearest_n = starling.get_nearest_n(flock)
    # test the output is of type `SubFlock
    assert(type(nearest_n) == starlings.SubFlock)
    # test that `n` nearest neighbours were returned
    if len(positions) >= n:
        assert(len(nearest_n.flock) == n)
    else:
        assert(len(nearest_n.flock) == len(positions))
    # test that the correct neighbours were returned, in the correct order
    for i, ind in enumerate(nearest_inds):
        assert(nearest_n.flock[i] == flock.flock[ind])
    assert flock.flock
    

@pytest.mark.parametrize('positions,velocities,a,b,c,d,roost,expected', [
    # no starlings in view, no roosting force
    (None, None, 1, 1, 1, 1, None, ZEROS),
    # no starlings in view, starling at roost (so no roosting force)
    (None, None, 1, 1, 1, 1, ZEROS, ZEROS),
    # no starlings in view, roosting force in one dimension
    (None, None, 1, 1, 1, 1, ONE_DIM, ONE_DIM),
    # no starlings in view, roosting force in all dimensions
    (None, None, 1, 1, 1, 1, np.array([1., 2., 3.]), np.array([1., 2., 3.])),
    # no starlings in view, roosting force in all dimensions, different roost factor
    (None, None, 1, 1, 1, 5, np.array([1., 2., 3.]), np.array([5., 10., 15.])),
    # one starling in view, no velocities, no repulsion, no roosting force
    ([ONES], None, 1, 1, 0, 1, None, ONES),
    # one starling in view, no velocities, no roost, one dim
    ([ONE_DIM], None, 1, 1, 1, 1, None, ZEROS),
    # one starling in view, no velocities, no roost, all dims
    ([ONES], None,  1, 1, 1, 1, None, (2 / 3) * ONES),
    # one starling in view, no position or repulsion force, no roost, one dim
    ([ONE_DIM], [ONE_DIM], 0, 1, 0, 1, None, ONE_DIM),
    # one starling in view, no repulsion or roost, one dim
    ([ONE_DIM], [ONE_DIM], 1, 1, 0, 1, None, 2 * ONE_DIM),
    # two starlings in view, opposing position and velocity, no replusion or 
    # roost, one dim
    ([ONE_DIM, -1 * ONE_DIM], [ONE_DIM, -1 * ONE_DIM], 1, 1, 0, 1, None, ZEROS),
    # two starlings in view, same position and velocity, no repulsion or roost,
    # all dims
    ([ONES, ONES], [ONES, ONES], 1, 1, 0, 1, None, 2 * ONES),
    # two starlings in view, different position and velocity, no repulsion or roost,
    # all dims
    ([ONES, -1 * ONES], [ONES, 2 * ONES], 1, 1, 0, 1, None, 1.5 * ONES),
    # two starlings in view, opposing positions, different velocities, cancelling 
    # replusion, no roost, all dims
    ([ONES, -1 * ONES], [ONES, 2 * ONES], 1, 1, 1, 1, None, 1.5 * ONES),
    # two starlings in view, opposing positions, different velocities, cancelling 
    # replusion, including roost, all dims
    ([ONES, -1 * ONES], [ONES, 2 * ONES], 1, 1, 1, 1, ONES, 2.5 * ONES),
    # two starlings in view, opposing positions, different velocities, cancelling 
    # replusion, including roost, all dims, different factors 
    ([ONES, -1 * ONES], [ONES, 2 * ONES], 0.1, 0.2, 0.3, 0.4, ONES, 0.7 * ONES),
])
def test_starling_get_forces(positions, velocities, a, b, c, d, roost, expected):
    """
    """
    starling = starlings.Starling(ZEROS, ZEROS, n=3)
    if positions or velocities:
        in_view = flock_factory(
            flock_type='subflock', positions=positions, velocities=velocities
        )
    else:
        in_view = starlings.SubFlock([])
    forces = starling.get_forces(in_view, a=a, b=b, c=c, d=d, roost=roost)
    np.testing.assert_allclose(forces, expected)


@pytest.mark.parametrize('min_speed,max_speed,forces,dt,expected', [
    # no forces
    (0., 1., ZEROS, 1., ZEROS),
    # small force in one dimension
    (0., 1., 0.1 * ONE_DIM, 1., 0.1 * ONE_DIM),
    # large force in one dimension
    (0., 1., 10 * ONE_DIM, 1., ONE_DIM),
    # large force in one dimension, large max speed
    (0., 10., 10 * ONE_DIM, 1., 10 * ONE_DIM),
    # small force in all dimensions
    (0., 1., 0.1 * ONES, 1., 0.1 * ONES),
    # large force in all dimensions
    (0., 1., ONES, 1., (1 / np.sqrt(3)) * np.ones(3)),
])
def test_starling_update_velocity(min_speed, max_speed, forces, dt, expected):
    """
    """
    starling = starlings.Starling(
        ZEROS, ZEROS, min_speed=min_speed, max_speed=max_speed
    )
    starling.update_velocity(forces, dt)
    np.testing.assert_equal(starling.new_velocity, expected)


@pytest.mark.parametrize('position,velocity,dt,expected', [
    # stationary
    (ZEROS, ZEROS, 1., ZEROS),
    # movement in one dimension
    (ZEROS, np.array([0., 0., 1.]), 1., np.array([0., 0., 1.])),
    # movement in all dimensions
    (ZEROS, ONES, 1., ONES),
    # movement in all dimensions different time increment
    (ZEROS, ONES, .1, 0.1 * ONES),
])
def test_starling_move(position, velocity, dt, expected):
    """
    """
    starling = starlings.Starling(position, velocity)
    starling.move(dt)
    np.testing.assert_equal(starling.position, expected)
    
 
@pytest.mark.parametrize('positions,attr,expected', [
    ([ZEROS, ONES], 'position', np.array([[0., 0., 0.], [1., 1., 1.]])),
])
def test_subflock_collect(positions, attr, expected):
    """
    """
    flock = flock_factory(flock_type='subflock', positions=positions)
    output = flock.collect(attr)
    np.testing.assert_equal(output, expected)


@pytest.mark.parametrize('positions,velocities,attr,expected', [
    # mean position
    ([ZEROS, ONES], None, 'position', 0.5 * ONES),
    # mean velocity
    (None, [ZEROS, ONES], 'velocity', 0.5 * ONES),
])
def test_subflock_flock_mean(positions, velocities, attr, expected):
    """
    """
    flock = flock_factory(
        flock_type='subflock', positions=positions, velocities=velocities
    )
    output = flock.flock_mean(attr)
    np.testing.assert_equal(output, expected)
    

@pytest.mark.parametrize('positions,velocities,span,view,exp_inds', [
    # one starling
    ([ZEROS], [ONES], np.pi/2, 1, {0: []}),
    # two starlings, first looking directly at second, second can't see first
    ([ZEROS, ONE_DIM], [ONE_DIM, ONE_DIM], np.pi/2, 2, {0: [1], 1: []}),
    # two starlings, second on span threshold of first, second can't see first
    ([ZEROS, ONE_DIM], [np.array([0., 1., 0.]), ONE_DIM], np.pi, 2, {0: [], 1: []}),
    # two starlings, second at view threshold of first, second can't see first
    ([ZEROS, ONE_DIM], [ONE_DIM, ONE_DIM], np.pi/2, 1, {0: [], 1: []}),
    # two starlings, second in view of first, second can't see first
    ([ZEROS, ONE_DIM], [np.array([1., 1., 0.]), ONE_DIM], np.pi, 2, {0: [1], 1: []}),
    # two starlings, neither in view of other
    ([ZEROS, ONE_DIM], [ONES, ONE_DIM], np.pi/2, 2, {0: [], 1: []}),
    # two starlings looking directly at each other and within range
    ([ZEROS, ONE_DIM], [ONE_DIM, -1 * ONE_DIM], np.pi/2, 2, {0: [1], 1: [0]}),
    # two starlings looking directly at each other but out of range
    ([ZEROS, ONE_DIM], [ONE_DIM, -1 * ONE_DIM], np.pi/2, 0.5, {0: [], 1: []}),
    # three starlings, first can see other two, second can see third only
    ([ZEROS, 0.5 * ONE_DIM, ONE_DIM], [ONE_DIM, ONE_DIM, ONE_DIM], np.pi/2, 2,
     {0: [1, 2], 1: [2], 2: []}),
    # three starlings in a line, all looking perpendicular to line, can't see
    # other starlings
    ([-1 * ONE_DIM, ZEROS, ONE_DIM], 
     [np.array([0., 1., 0.]), np.array([0., 1., 0.]), np.array([0., 1., 0.])],
     np.pi/2, 2, {0: [], 1: [], 2: []}
    ),
    # three starlings in a line, all looking perpendicular to line, can see
    # other starlings
    ([ZEROS, 0.5 * ONE_DIM, ONE_DIM], 
     [np.array([0., 1., 0.]), np.array([0., 1., 0.]), np.array([0., 1., 0.])],
     4 * np.pi / 3, 2, {0: [1, 2], 1: [0, 2], 2: [0, 1]}
    ),
])
def test_flock_get_all_in_view(positions, velocities, span, view, exp_inds):
    """
    """
    flock = flock_factory(
        positions=positions, velocities=velocities, span=span, view=view
    )
    output = flock.get_all_in_view()
    expected = {}
    for i in exp_inds:
        expected[flock.flock[i]] = (
            starlings.SubFlock([flock.flock[j] for j in exp_inds[i]])
        )
    # as different SubFlock objects are created in .get_all_in_view() and in
    # generation of the `expected` dict despite containing the same starlings, 
    # need to compare the flock attribute from each of these, rather than the 
    # overall subflock
    for i in output:
        assert(output[i].flock == expected[i].flock)
    # reverse the test
    for i in expected:
        assert(expected[i].flock == output[i].flock)


@pytest.mark.parametrize(
    'positions,velocities,dt,roost,expected_ts1,expected_ts2', [
    # one starling, no roost, zero velocity
    ([ZEROS], [ZEROS], 1., None, np.array([ZEROS]), np.array([ZEROS])),
    # one starling, no roost, non-zero initial velocity
    ([ZEROS], [0.1 * ONES], 1., None, 0.1 * np.array([ONES]), 
     0.2 * np.array([ONES])),
    # one starling, roost, non-zero initial velocity
    ([ZEROS], [-0.1 * ONE_DIM], 1., ONE_DIM, 0.9 * np.array([ONE_DIM]), 
     1.9 * np.array([ONE_DIM])),
    # two starlings, moving in one dimension, roost
    ([0.5 * ONE_DIM, ONE_DIM], [ONE_DIM, ONE_DIM], 1., -1 * ONE_DIM, 
     np.array([-0.5 * ONE_DIM, ZEROS]), 
     np.array([-1.5 * ONE_DIM, -0.5 * ONE_DIM]))
])
def test_flock_update(
        positions, velocities, dt, roost, expected_ts1, expected_ts2
    ):
    """
    """
    flock = flock_factory(
        positions=positions, velocities=velocities, dt=dt, roost=roost
    )
    # iterate over two time steps, check flock positions output at each
    flock.update()
    np.testing.assert_equal(flock.collect('position'), expected_ts1)
    flock.update()
    np.testing.assert_equal(flock.collect('position'), expected_ts2)
    
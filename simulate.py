import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from starlings import starlings


class Simulation:
    """
    Instantiates a simulation based on the parameters provided. Contains a 
    method to integrate the simulation forward in time, and return the results.
    
    Args:
        flock_size (int): the number of starlings in the flock.
        frames (float): how many frames (time-slices) the simulation will 
            contain.
        **kwargs: refer to `Starling` and `Flock` classes documentation for 
            optional keyword arguments.
    """
    def __init__(self, flock_size, frames, **kwargs):
        self.flock_size = flock_size
        self.frames = frames
        # separate `Flock` kwargs from `Starling` kwargs
        kwargs_copy = kwargs.copy()
        flock_kwargs = {}
        for i in kwargs_copy:
            if i in ['dt', 'a', 'b', 'c', 'd', 'roost']:
                flock_kwargs[i] = kwargs_copy[i]
                del kwargs[i]
        # create a flock of starlings
        flock = []
        for i in range(self.flock_size):
            position = np.random.uniform(low=-1., size=3)
            velocity = 0.001 * np.random.uniform(low=-1., size=3)
            flock.append(starlings.Starling(position, velocity, **kwargs))
        self.flock = starlings.Flock(flock, **flock_kwargs)
        
    def run(self):
        """
        Runs the simulation and returns the results.
        
        Returns:
            data (numpy.array): the results of the simulation - a three 
            dimensional array consisting of positional vectors for each starling 
            at each timestep. With the outermost dimension first, dimensions of 
            `data` are ordered as: time, starling, position.
        """
        data = np.zeros((self.frames, self.flock_size, 3))
        data[0] = self.flock.collect('position')
        # run the simulation
        for i in range(1, self.frames):
            self.flock.update()
            data[i] = self.flock.collect('position')
        return data
    
    @staticmethod
    def make_animation(data):
        """
        """
        # to be passed to matplotlib.animation.FuncAnimation instance
        def init():
            """Initialises a scatter plot."""
            scat.set_data(data[0,:,0], data[0,:,1])
            return scat,
        # to be passed to matplotlib.animation.FuncAnimation instance
        def animate(i):
            """Updates the scatter plot with index `i`."""
            scat.set_data(data[i,:,0], data[i,:,1])
            return scat,
        # initialise the figure, and create animation
        fig = plt.figure()
        lim = max(abs(np.min(data)), np.max(data))
        plt.axes(xlim=(-lim, lim), ylim=(-lim, lim))
        scat, = plt.plot([], [], 'ko', markersize=3)
        ani = animation.FuncAnimation(
            fig, animate, init_func=init, frames=range(1, len(data)), 
            interval=20
        )
        return ani

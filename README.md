# About

This is a program that attempts to accurately simulate 'murmurations', as 
displayed by flocks of starlings and other types of birds in nature, in three
dimensions. 

Note that it doesn't actually produce a visual animation, but instead just runs 
the simulation and returns the resulting data. It's up to the user
to animate the results separately. This can be done easily with the `animate` 
module within `matplotlib`, but first you'll need to project the results onto 
two dimensions (i.e. chop out one of the three spatial dimensions from the results).

# Usage

There are two modules within the `starlings` package: `starlings` and `simulate`.

`starlings.py`: the `starlings` module contains the `Starling` and `Flock` 
classes, which deal with the physics and group dynamics of the problem. Take a 
look at the `Starling` doc to see what physical parameters an individual starling 
can take on. Further physical parameters governing flock behaviour as a whole 
are described in the `Flock` doc.


`simulate.py`: producing a simulation is done exclusively via this module. It
contains the `Simulate` class. To run a basic simulation, start a Python session 
and run:

```
from starling import simulate

flock_size = 100
no_frames = 100
sim = simulate.Simulation(flock_size, no_frames)
result = sim.run()
```

This example produces a 100-frame simulation of a flock containing 100 starlings
(fixed in three dimensions). The results are stored in the `result` variable as 
a `numpy.array`. which will have shape (`frames`, `flock_size`, 3). Optional 
physical parameters used by the `Starling` and `Flock` classes can be supplied 
as kwargs to the `Simulation` instance to modify the observed behaviour.

Note that computation time is $`O(kn^2)`$, where $`k`$ is the number of frames,
and $`n`$ is the number of starlings (each starling must look at every other 
starling to determine its movement at each time step). As a result, the simulation 
gets *very* sluggish *very* quickly as the flock size is increased. Try starting 
off with 500 starlings to see if performance is acceptable, and adjust as necessary.

# Suggested simulation settings

Still trying to figure this out...

# Dependencies

All it needs to work is `numpy` (and your animation package of choice if you want
to animate the results).
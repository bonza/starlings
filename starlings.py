import numpy as np
from scipy.spatial import cKDTree


class Starling:
    """
    Single instance of a starling.
    
    Args:
        position (np.array): initial position vector, three-dimensional.
        velocity (np.array): initial velocity vector, three-dimensional.
        min_speed (float): minimum possible flight speed.
        max_speed (float): maximum possible flight speed.
        span (float): angular span of sight, in radians.
        view (float): distance range of sight.
        n (int): number of nearest starlings this starling follows.
    """
    def __init__(
            self, position, velocity, min_speed=0., max_speed=1., span=np.pi/2, 
            view=1., n=1
        ):
        self.position = position.copy()
        self.velocity = velocity.copy()
        self.max_speed = max_speed
        self.min_speed = min_speed
        self.span = span
        self.view = view
        self.n = n
        
    def get_direction_to_other(self, other):
        """
        Determines the relative distance and angle from this starling to 
        another starling.
        
        Args:
            other (Starling): the other starling.
            
        Returns:
            (dist, ang): the distance and angle (radians) from this starling to
                the other starling.
        """
        diff = other.position - self.position
        # euclidean distance between the two starlings
        dist = np.linalg.norm(diff)
        # angle from this starling to the other relative to this starling's
        # orientation (direction of movement)
        arg = np.sum(diff * self.velocity) / (dist * np.linalg.norm(self.velocity))
        ang = np.arccos(np.round(arg, 9))
        return (dist, ang)
    
    def get_nearest_n(self, in_view):
        """
        Returns a SubFlock containing the closest `self.n` starlings that are
        within view of this starling.
        
        Args:
            in_view (SubFlock): all starlings that are in view of this starling.
        
        Returns:
            nearest (SubFlock): the closest `self.n` starlings to this starling
                from `in_view`.
        """
        nearest = []
        for i in in_view.flock:
            dist = np.linalg.norm(i.position - self.position)
            nearest.append((i, dist))
        # sort by distance to this starling (closest to furthest)
        nearest = sorted(nearest, key=lambda x: x[1])
        # take the closest `neighbours` starlings if surplus
        if len(nearest) > self.n:
            nearest = nearest[:self.n]
        # pull out the starling objects, pack into a SubFlock
        nearest = SubFlock([i[0] for i in nearest])
        return nearest
        
    def get_forces(self, in_view, a, b, c, d, roost=None):
        """
        Determines the net instantaneous force acting on this starling.
        
        Args:
            in_view (SubFlock): all starlings that are in view of this starling.
            a (float): scalar governing strength of attraction of this starling
                to the mean relative position of its neighbours.
            b (float): scalar governing strength of attraction of this starling
                to the mean relative velocity of its neighbours.
            c (float): scalar governing strength of repulsion of this starling
                away from each of its neighbours due to proximity.
            d (float): scalar governing strength of attraction to some fixed 
                relative 'roost' location within the domain. 
            roost (numpy.array or None): the location of the roost.
            
        Returns:
            forces (numpy.array): the net force acting on this starling in each
                dimension.
        """
        forces = np.zeros(3)
        if in_view.flock.size > 0:
            # `nearest_n` is a `SubFlock` object
            nearest_n = self.get_nearest_n(in_view)
            # force due to attraction to mean position of nearest n starlings
            # (proportional to size of positional difference)
            positions = nearest_n.collect("position")
            forces += a * (np.mean(positions, axis=0) - self.position)
            # force due to attraction to mean velocity of nearest n starlings
            # (proportional to size of velocity difference)
            forces += b * (nearest_n.flock_mean("velocity") - self.velocity)
            diff = positions - self.position
            dist = np.sqrt(np.sum(diff ** 2, axis=1))
            # add outer dimension to distances array for array operations
            dist = np.array([dist]).T
            # repulsion due to proximity of each starling in view (proportional 
            # to the inverse square distance to each starling)
            forces += -c * np.sum(diff / dist ** 2, axis=0)
        # apply roosting force
        if roost is not None:
            forces += d * (roost - self.position)
        return forces
    
    def update_velocity(self, forces, dt):
        """
        Updates this starling's velocity based on the net instantaneous forces
        acting on it.
        
        Args:
            forces (numpy.array): the net forces acting on this starling, as
                calculated by the `self.get_forces` method.
            dt (float): time increment.
        """
        velocity = self.velocity + forces * dt
        speed = np.linalg.norm(velocity)
        # clip speed to within allowed range if beyond this
        if speed > self.max_speed:
            self.new_velocity = self.max_speed * velocity / speed
        elif speed < self.min_speed:
            self.new_velocity = self.min_speed * velocity / speed
        else:
            self.new_velocity = velocity
        
    def move(self, dt):
        """
        Updates this starling's position based on its current velocity.
        
        Args:
            dt (float): time increment.
        """
        self.position += dt * self.velocity


class SubFlock:
    """
    A collection of starlings. Provides methods to calculate mean features over
    the flock.
    
    Args:
        starlings (list of Starling): list of starlings comprising the flock.
    """
    def __init__(self, starlings):
        self.flock = np.array(starlings)
        
    def collect(self, attr):
        """
        Collect data for a given attribute for each starling in the flock, and
        pack into a numpy array.
        
        Args:
            attr (str): the attribute of interest.
            
        Returns:
            data (numpy.array): `attr` data for each starling in the flock.
        """
        data = []
        for starling in self.flock:
            data.append(getattr(starling, attr))
        data = np.array(data)
        return data
    
    def flock_mean(self, attr):
        """
        Computes the flock-wide mean of an attribute.
        
        Args:
            attr (str): the attribute of interest.
            
        Returns:
            result (numpy.array): the mean `attr` data for the entire flock in
                each dimension.
        """
        data = self.collect(attr=attr)
        result = np.mean(data, axis=0)
        return result
    
    
class Flock(SubFlock):
    """
    A flock of starlings. Inherits `SubFlock` class, plus methods to integrate
    the flock dynamics forwards in time.
    
    Args:
        starlings (list of Starling): list of starlings comprising the flock.
        dt (float): time-integration increment.
        a (float): scalar governing strength of attraction of each starling
            to the mean relative position of its neighbours.
        b (float): scalar governing strength of attraction of each starling
            to the mean relative velocity of its neighbours.
        c (float): scalar governing strength of repulsion of each starling
            away from each of its neighbours due to proximity.
        d (float): scalar governing strength of attraction of each starling to 
            some fixed relative 'roost' location within the domain. 
        roost (numpy.array or None): the location of the roost.
    """
    def __init__(
        self, starlings, dt=0.1, a=1, b=1, c=1, d=1, 
        roost=np.array([0., 0., 0.])):
        SubFlock.__init__(self, starlings)
        self.dt = dt
        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.roost = roost
        self.views = self.collect('view')
        
    def get_all_in_view(self):
        """
        For each starling in the flock, finds all starlings that are in its 
        view.
        
        Returns:
            in_view_dict (dict): dictionary whose keys consist of each individual 
                starling within the flock, and whose items are subflocks of all
                starlings in view of that particular starling key.
        """
        # create a kdtree to speed up the initial spatial query
        positions = self.collect('position')
        tree = cKDTree(positions)   
        neighbours_array = tree.query_ball_point(positions, self.views)
        #
        in_view_dict = {}
        for i in range(len(self.flock)):
            subject = self.flock[i]
            neighbour_inds = neighbours_array[i]
            # remove reference to self
            neighbour_inds.remove(i)
            neighbours = self.flock[neighbour_inds]
            in_view = []
            for j in neighbours:
                # if starling j is within starling i's angle of sight, 
                # append to list
                _, ang = subject.get_direction_to_other(j)
                # divided by 2 as span is centred on line of sight
                if (ang < subject.span / 2):
                    in_view.append(j)
            # record all starlings in view of starling i as a new SubFlock
            in_view_dict[subject] = SubFlock(in_view)
        # import pdb; pdb.set_trace()
        return in_view_dict                    
            
    def update(self):
        """
        Updates the acceleration, velocity, and position of each starling in the
        flock by one time iteration.
        """
        in_view_dict = self.get_all_in_view()
        # first loop records updated velocity as a new variable for each starling
        for i in in_view_dict:
            # `in_view` and `nearest_n` are both `SubFlock` objects
            in_view = in_view_dict[i]
            # get forces acting on `i` based on all starlings within its view
            forces = i.get_forces(
                in_view, a=self.a, b=self.b, c=self.c, d=self.d, roost=self.roost
            )
            # update `i`'s velocity based on the computed force field
            i.update_velocity(forces, self.dt)
        # second loop implements updated velocity, moves starling accordingly
        for i in in_view_dict:
            i.velocity = i.new_velocity
            delattr(i, "new_velocity")
            # update starling's position
            i.move(self.dt)
            